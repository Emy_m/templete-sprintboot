package com.example.demo.controller;

import com.example.demo.controller.dto.UserDTO;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    final static Logger logger = LoggerFactory.getLogger(UserController.class);


    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDTO> all() {
        logger.warn("find all");
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public UserDTO find(@PathVariable Long id) {
        return userService.find(id);
    }

    @PostMapping
    public UserDTO save(@Valid @RequestBody UserDTO userDTO) {
        logger.warn("add user");
        return userService.save(userDTO);
    }

    @PutMapping("/{id}")
    public UserDTO update(@PathVariable Long id, @Valid @RequestBody UserDTO userDTO) {
        return userService.update(id, userDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        userService.delete(id);
    }
}
